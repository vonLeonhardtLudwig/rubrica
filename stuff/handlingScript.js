let number
let name
let type
let id
let idTel
let type_values = ["cellphone", "phoneNumber", "fax"]
$(document).ready(function() {
    number = $("#contactNumber")[0]
    name = $("#contactName")[0]
    type = $("#types")[0]
    id = $("#idContact")[0]
    idTel = $("#idTelephone")[0]
    let digits = $(".digit")
    for (let i = 0; i < 12; i++) {
        digits[i].onclick = clickDigit
    }
    let info = $(".info")
    for (let i = 0; i < info.length; i++) {
        info[i].onclick = clickInfo
    }

    $("#contactName").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#contactsList .contactRow").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

function clickDigit() {
    number.value += this.children[0].value
}

function clickInfo() {
    let info_name = this.children[0]
    let info_type = this.children[1]
    let info_numb = this.children[2]
    let info_class = this.parentNode.classList[1]
    let idNum = this.id
    name.value = info_name.innerHTML
    number.value = info_numb.innerHTML
    id.value = info_class
    idTel.value = idNum
    switch (info_type.innerHTML) {
        case "Cellphone":
            type.value = type_values[0]
            break;
        case "Phone Number":
            type.value = type_values[1]
            break;
        case "Fax":
            type.value = type_values[2]
    }
}