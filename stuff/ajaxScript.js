$(document).ready(function() {
    $("#addContact").click(function(event) {
        event.preventDefault();
        let post_url = "/add"
        let req_method = "post"
        let form_data = $("#contactForm").serialize();
        $.ajax({
            url: post_url,
            type: req_method,
            data: form_data,

        }).then((data) => {
            console.log(data)
            addNum(data)
        })
    })

    $("#updateName").click(function(event) {
        event.preventDefault();
        let post_url = "/updateName"
        let req_method = "put"
        let idCon = $("#idContact")[0].value
        let form_data = $("#contactForm").serialize();
        $.ajax({
            url: post_url,
            type: req_method,
            data: form_data,

        }).then((data) => {
            console.log(idCon)
            console.log(data)
            let infos = $("." + idCon).children()
            for (let i = 0; i < infos.length; i++) {
                infos[i].children[0].innerHTML = data.contactName
            }
        })
    })

    $("#updateType").click(function(event) {
        event.preventDefault();
        let post_url = "/updateType"
        let req_method = "put"
        let idTel = $("#idTelephone")[0].value
        let form_data = $("#contactForm").serialize();
        $.ajax({
            url: post_url,
            type: req_method,
            data: form_data,

        }).then((data) => {
            console.log(idTel)
            console.log(data)
            let num = $("#" + data.idTelephone)[0]
            switch (data.contactType) {
                case "cellphone":
                    num.children[1].innerHTML = "Cellphone"
                    break;
                case "phoneNumber":
                    num.children[1].innerHTML = "Phone Number"
                    break;
                case "fax":
                    num.children[1].innerHTML = "Fax"
            }
        })
    })


    $("#updateNumber").click(function(event) {
        event.preventDefault();
        let post_url = "/updateNumber"
        let req_method = "put"
        let idTel = $("#idTelephone")[0].value
        let form_data = $("#contactForm").serialize();
        $.ajax({
            url: post_url,
            type: req_method,
            data: form_data,

        }).then((data) => {
            console.log(idTel)
            console.log(data)
            let num = $("#" + data.idTelephone)[0]
            num.children[2].innerHTML = data.contactNumber
        })
    })

    $("#deleteContact").click(function(event) {
        event.preventDefault();
        let post_url = "/deleteCon"
        let req_method = "delete"
        let form_data = $("#contactForm").serialize();
        $.ajax({
            url: post_url,
            type: req_method,
            data: form_data,
            success: function(data) {
                console.log(data)
                $('.' + data.idContact).empty()
                $('.' + data.idContact).remove()
            }
        })
    })

    $("#deleteNumber").click(function(event) {
        event.preventDefault();
        let post_url = "/deleteNum"
        let req_method = "delete"
        let idTel = $("#idTelephone")[0].value
        let form_data = $("#contactForm").serialize();
        $.ajax({
            url: post_url,
            type: req_method,
            data: form_data,
            success: function(data) {
                console.log(data)
                $('#' + data).empty()
                $('#' + data).remove()
            }
        })
    })



})

function addNum(data) {
    let contactRow = document.createElement("div")
    contactRow.className = "contactRow"

    let info = document.createElement("div")
    info.className = "info"

    let name = document.createElement("div")
    name.className = "element"
    name.innerHTML = data.contactName

    let type = document.createElement("div")
    type.className = "element"
    type.innerHTML = data.contactType

    let number = document.createElement("div")
    number.className = "element"
    number.innerHTML = data.contactNumber



    info.append(name)

    info.append(type)
    info.append(number)
    contactRow.append(info)

    $('#listHeader').after(contactRow)
}