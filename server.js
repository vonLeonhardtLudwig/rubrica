const express = require("express");
const app = express();
const Sequelize = require('sequelize');
const bodyParser = require('body-parser')

const fs = require('fs');

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/stuff'));

let allOverCSS = { style: fs.readFileSync('./stuff/allOver.css', 'utf8') };
let azioniContattiCSS = { style: fs.readFileSync('./stuff/azioniContatti.css', 'utf8') };
let listaContattiCSS = { style: fs.readFileSync('./stuff/listaContatti.css', 'utf8') };
let tastierinoCSS = { style: fs.readFileSync('./stuff/tastierino.css', 'utf8') };

let handlingScript = { script: fs.readFileSync('./stuff/handlingScript.js', 'utf8') };
let ajaxScript = { script: fs.readFileSync('./stuff/ajaxScript.js', 'utf8') };


app.use(bodyParser.urlencoded({ extended: true }));

let sequelize = new Sequelize('RubricaDB', 'rubrica', 'rubrica@2020', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

const Contact = sequelize.import(__dirname + '/models/Contact.js');
Contact.sync();
const Telephone = sequelize.import(__dirname + '/models/Telephone.js');
Telephone.sync();

Telephone.belongsTo(Contact);
Contact.hasMany(Telephone, { onDelete: 'CASCADE' });

//sequelize.sync({ force: true });
sequelize.sync();

let allInfos = [];


function getAllContacts() {
    return sequelize.query("SELECT Contacts.idContact, Contacts.contactName, Telephones.idTelephone, Telephones.telephoneType, Telephones.telephoneNumber FROM Contacts, Telephones WHERE Contacts.idContact = Telephones.ContactIdContact ORDER BY Contacts.contactName;", { type: sequelize.QueryTypes.SELECT });
}

function prepareMatrix(res) {
    return new Promise((resolve, reject) => {
        allInfos = []
        let list = res
        for (let i = 0, j = 0; i < list.length; i++) {
            if (i > 0) {
                if (list[i].idContact == list[i - 1].idContact) {
                    allInfos[j].push(list[i])
                } else {
                    allInfos.push([])
                    j++;
                    allInfos[j].push(list[i])
                }
            } else {
                allInfos.push([])
                allInfos[j].push(list[i])
            }
        }
        resolve(console.log(allInfos))
    })
}



async function dlPage(res) {
    let res_foo = await getAllContacts()
    let res_foo2 = await prepareMatrix(res_foo).then(() => {
        console.log(allInfos)
        res.render('index.ejs', {
            allInfos: allInfos,
            allOverCSS: allOverCSS,
            azioniContattiCSS: azioniContattiCSS,
            listaContattiCSS: listaContattiCSS,
            tastierinoCSS: tastierinoCSS,
            handlingScript: handlingScript,
            ajaxScript: ajaxScript
        })
    })


}

app.get('/', function(req, res) {
    dlPage(res)
})


app.put('/updateNumber', (req, res) => {
    let idTelephone = req.body.idTelephone;
    let contactNumber = req.body.contactNumber
    Telephone.update({ telephoneNumber: contactNumber }, { where: { idTelephone: idTelephone } }).then(res.send(req.body))
})

app.put('/updateName', (req, res) => {
    let idContact = req.body.idContact
    let contactName = req.body.contactName
    Contact.update({ contactName: contactName }, { where: { idContact: idContact } }).then(res.send(req.body))
})

app.put('/updateType', (req, res) => {
    let idTelephone = req.body.idTelephone;
    let type = req.body.contactType
    Telephone.update({ telephoneType: type }, { where: { idTelephone: idTelephone } }).then(res.send(req.body))

})

app.delete('/deleteCon', (req, res) => {
    console.log(req.body)
    let contactName = req.body.contactName
    let contactType = req.body.contactType
    let contactNumber = req.body.contactNumber
    let contactId = req.body.idContact
    Contact.findOne({ where: { idContact: contactId } }).then((c) => {
        Contact.destroy({ where: { idContact: contactId } }).then(
            Telephone.destroy({ where: { ContactIdContact: null } }).then(
                res.send(c)
            )
        )
    })

})


app.delete('/deleteNum', (req, res) => {
    let telId = req.body.idTelephone
    Telephone.destroy({ where: { idTelephone: telId } }).then(res.send(telId))
})


function addNumberToDb_(name, type, numb) {
    return new Promise((resolve, reject) => {
        Contact.findOrCreate({
            where: { contactName: name },
            defaults: {}
        }).then(() => {
            Telephone.create({ telephoneType: type, telephoneNumber: numb }).then((tel) => {
                Contact.findOne({ where: { contactName: name } }).then((con) => {
                    con.addTelephone(tel);
                })
            })
        })

    })
}

async function addNumber(name, type, numb, res) {
    let res_foo = await addNumberToDb_(name, type, numb).then(console.log("done"))
}

app.post('/add', function(req, res) {
    let name = req.body.contactName
    let type = req.body.contactType
    let numb = req.body.contactNumber
    console.log(name, type, numb)
    addNumber(name, type, numb, res);
    res.send(req.body)


})

app.listen(3000, () => {
    console.log('Server ready')
})