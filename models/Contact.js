module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Contact', {
        idContact:{
            type: DataTypes.INTEGER(11),
            unique: true,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        contactName:{
            type: DataTypes.CHAR(32),
            allowNull: false,
            defaultValue: ''
        }
    },{
        freezeTableName: true,
        tableName: 'Contacts'
    })

};