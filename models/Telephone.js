module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Telephone', {
        idTelephone:{
            type: DataTypes.INTEGER(11),
            unique: true,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        telephoneType:{
            type: DataTypes.ENUM('cellphone', 'phoneNumber','fax'),  
            allowNull: false, 
            defaultValue: 'cellphone'     
        },
        telephoneNumber:{
            type: DataTypes.CHAR(32),
            unique: true,
            allowNull: false,
            defaultValue: ''
        }
    },{
        freezeTableName: true,
        tableName: 'Telephones'
    })

};